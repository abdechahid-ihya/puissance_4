/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aihya <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/23 11:30:46 by aihya             #+#    #+#             */
/*   Updated: 2018/12/23 22:46:46 by aihya            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct	s_col
{
	char			*data;
	int				full;
	int				pos;
	struct s_col	*next;
	struct s_col	*prev;
}				t_col;

typedef struct	s_board
{
	t_col	*col;
	int		cols;
	int		rows;
}				t_board;

t_col	*new_node(int rows, t_col *prev)
{
	t_col	*node;
	int		i;

	node = (t_col *)malloc(sizeof(t_col));
	node->next = NULL;
	node->prev = NULL;
	if (prev)
		node->prev = prev;
	node->data = (char *)malloc(sizeof(char) * (rows + 1));
	i = 0;
	while (i < rows)
	{
		node->data[i] = '.';
		i++;
	}
	node->data[i] = '\0';
	node->full = 0;
	return (node);
}

t_board	*creat_board(int cols, int rows)
{
	t_board	*board;
	t_col	*head;
	t_col	*curr;
	int		i;

	head = new_node(rows, NULL);
	curr = head;
	i = 0;
	while (++i < cols)
	{
		curr->next = new_node(rows, curr);
		curr = curr->next;
	}
	board = (t_board *)malloc(sizeof(t_board));
	board->col = head;
	board->cols = cols;
	board->rows = rows;
	return (board);
}

int		analyze_d(int col, t_board *board, char ch)
{
	int		i;
	int		c;
	t_col	*_col;

	_col = board->col;
	while (_col->pos != col)
		_col = _col->next;
	i = 0;
	while (_col->data[i] == '.' && i != -1)
	{
		i++;
		if (_col->data[i] == '\0')
			i = -1;
	}
	if (i != -1)
	{
		c = 0;
		i++;
		while (i < board->rows)
		{
			if (_col->board[i] == ch)
				c++;
			else
				break;
			i++;
		}
		if (c == 4)
			return ();
	}
}

int		analyze_board(t_board *board, int turn)
{
	int		col;
	char	ch;

	col = 0;
	ch = (turn == 1) ? 'O' : 'X';
	while (col < board->cols)
	{
		if (analyze_d(col, board, ch) || analyze_l(col, board, ch)
		|| analyze_r(col, board, ch) || analyze_tl(col, board, ch)
		|| analyze_dl(col, board, ch) || analyze_tr(col, board, ch)
		|| analyze_dr(col, board, ch))
			return (1);
		col++;
	}
}

int		is_full(int col, t_board *board)
{
	t_col	*_col;
	int		i;

	_col = board->col;
	i = 0;
	while (i != col)
	{
		_col = _col->next;
		i++;
	}
	if (_col->full)
		return (1);
	return (0);
}

void	player(t_board *board)
{
	char	*buf;

	printf(">> ");
	fgets(buf, sizeof(buf), stdin);
	while (is_full(atoi(buf), board))
	{
		printf("FULL\n>> ");
		fgets(buf, sizeof(buf), stdin);
	}
}

void	start(t_board *board)
{
	int		turn;

	turn = 1;
	while (analyze_board(board, turn))
	{
		if (turn == (-1))
			ai(board);
		else
			player(board);
		turn = turn * (-1);
	}
}

int		main(int argc, char **argv)
{
	t_board	*board;
	t_col	*col;
	int		i;

	board = creat_board(atoi(argv[1]), atoi(argv[2]));
	start(board);
	i = 0;
	while (i < atoi(argv[2]))
	{
		col = board->col;
		while (col)
		{
			printf("\033[4m\033[2m|   ");
			col = col->next;
		}
		printf("\033[2m|\033[0m\n");
		i++;
	}

	return (0);
}
